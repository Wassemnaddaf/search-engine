import glob
from operator import itemgetter

import soundex
from nltk.corpus import stopwords, wordnet
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from collections import Counter, OrderedDict
from num2words import num2words
import checker
import date
from index import build_index
import nltk
import os
import string
import numpy as np
import copy
import pandas as pd
import pickle
import re
import math
from utils import *
from scipy import spatial


tokenized_corpus = []
tokenized_corpus_modified = []
corpus = []
DF = {}
N = 423
tf_idf = {}
D = []


def preprocess(data):
    # data = cleaning(data)
    data = convert_lower_case(data)
    data = handle_special_cases(data)
    data = remove_punctuation(data)
    data = remove_apostrophe(data)
    data = remove_stop_words(data)
    data = convert_numbers(data)
    data = lemming(data)
    data = convert_numbers(data)
    data = lemming(data)
    data = remove_punctuation(data)
    data = remove_stop_words(data)
    return data


def extract_data():
    my_files = glob.glob("corpus/*.txt")
    global tokenized_corpus, corpus, tokenized_corpus_modified
    tokenized_corpus = []
    tokenized_corpus_modified = []
    corpus = []
    i = 1
    for file_name in range(N):
        f = open("corpus/" + str(i) + '.txt', 'r')
        data = f.read()
        corpus.append(data)
        # tokenized_corpus.append(preprocess_modified(data))
        tokenized_corpus_modified.append(preprocess_modified(data))
        tokenized_corpus.append(word_tokenize(str(preprocess(data))))
        f.close()
        i = i + 1
        if i > N:
            break


def calc_df():
    global DF
    DF = {}
    for i in range(N):
        tokens = tokenized_corpus[i]
        for w in tokens:
            try:
                DF[w].add(i)
            except:
                DF[w] = {i}


    for i in DF:
        DF[i] = len(DF[i])


def doc_freq(word):
    c = 0
    try:
        c = DF[word]
    except:
        pass
    return c


def calc_tf_idf():
    doc = 0
    global tf_idf
    tf_idf = {}
    for i in range(N):

        tokens = tokenized_corpus[i]

        counter = Counter(tokens)
        words_count = len(tokens)

        for token in np.unique(tokens):
            tf = counter[token] / words_count
            df = doc_freq(token)
            idf = np.log((N + 1) / (df + 1))

            tf_idf[doc, token] = tf * idf

        doc += 1


def matching_score(k, query):
    preprocessed_query = preprocess(query)
    tokens = word_tokenize(str(preprocessed_query))

    print("Matching Score")
    print("\nQuery:", query)
    print("")
    print(tokens)

    query_weights = {}

    for key in tf_idf:

        if key[1] in tokens:
            try:
                query_weights[key[0]] += tf_idf[key]
            except:
                query_weights[key[0]] = tf_idf[key]

    query_weights = sorted(query_weights.items(), key=lambda x: x[1], reverse=True)

    print("")

    l = []

    for i in query_weights[:k]:
        l.append(i[0])

    print(l)


def cosine_sim(a, b):
    cos_sim = np.dot(a, b) / (np.linalg.norm(a) * np.linalg.norm(b))
    return cos_sim


def gen_vector(tokens):
    Q = np.zeros((len(total_vocab)))

    counter = Counter(tokens)
    words_count = len(tokens)

    query_weights = {}

    for token in np.unique(tokens):

        tf = counter[token] / words_count
        df = doc_freq(token)
        idf = math.log((N + 1) / (df + 1))

        try:
            ind = total_vocab.index(token)
            Q[ind] = tf * idf
        except:
            pass
    return Q


def get_similarity(vector):
    return vector.get('similarity')


def cosine_similarityy(k, query):
    # tokens = preprocess_modified(query)
    tokens = word_tokenize(str(preprocess(query)))

    print(tokens)

    d_cosines = []

    query_vector = gen_vector(tokens)

    for i in range(len(D)):
        similarity = cosine_sim(query_vector, D[i])
        if similarity > 0:
            d_cosines.append({"similarity": similarity, "file": i + 1})

    d_cosines.sort(key=get_similarity, reverse=True)

    print("")

    return d_cosines[:k]


def vectorization():
    global D
    D = np.zeros((N, total_vocab_size))
    for i in tf_idf:
        try:
            ind = total_vocab.index(i[1])
            D[i[0]][ind] = tf_idf[i]
        except:
            pass


def get_query_correction(query):
    suggests = []
    query_tokens = word_tokenize(str(preprocess(query)))
    for token in query_tokens:
        current = checker.spelling_check(DF, token)
        if current:
            suggests.append({token: current[:5]})

    return suggests


def auto_complete(current):
    result = []
    for i in range(len(tokenized_corpus)):
        if current in tokenized_corpus[i] and current not in result:
            result.append(tokenized_corpus[i])

    return result


extract_data()
calc_df()
total_vocab_size = len(DF)
total_vocab = [x for x in DF]

calc_tf_idf()
vectorization()


# result , vectors = build_index(tokenized_corpus)
corrections = checker.spelling_check(DF, "wassim")





Q = cosine_similarityy(30, "SPAIN'S RELAXATION OF CONTROLS OVER SOME OF ITS AFRICAN TERRITORIES .")
# dataFrame = pd.DataFrame(tf_idf)
# print(dataFrame)
