import soundex
from nltk.corpus import stopwords, wordnet
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from num2words import num2words
import date
import nltk
import string
import numpy as np
import re
from nltk.stem import WordNetLemmatizer, LancasterStemmer


shortcuts = {
    "us": "united_state",
    "u.s": "united_state",
    "united state": "united_state",
    "un": "united_nations",
    "u.n": "united_nations",
    "united nations": "united_nations",
    "sa": "south_africa",
    "s.a": "south_africa",
    "south africa": "south_africa",
    "usa": "united_state"
}
wordnet_lemmatizer = nltk.WordNetLemmatizer()
stemmer = LancasterStemmer()
porter_stemmer = PorterStemmer()
stop_words = stopwords.words('english')


def convert_lower_case(data):
    return np.char.lower(data)


def remove_apostrophe(data):
    return np.char.replace(data, "'", "")


def remove_stop_words(data):
    words = word_tokenize(str(data))
    new_text = ""
    for w in words:
        if w not in stop_words and len(w) > 1:
            new_text = new_text + " " + w
    return new_text


def remove_punctuation(data):
    symbols = "!\"#$%&()*+-./:;<=>?@[\]^_`{|}~\n"
    for i in symbols:
        data = np.char.replace(data, i, ' ')
    return data


def stemming(data):
    tokens = word_tokenize(str(data))
    new_text = ""
    for w in tokens:
        new_text = new_text + " " + stemmer.stem(w)
    return data


def lemming(data):
    tag_map = nltk.defaultdict(lambda: wordnet.NOUN)
    tag_map['J'] = wordnet.ADJ
    tag_map['V'] = wordnet.VERB
    tag_map['R'] = wordnet.ADV
    tokens = word_tokenize(str(data))

    filtered_sentence = []

    for token, tag in nltk.pos_tag(tokens):
        filtered_sentence.append(wordnet_lemmatizer.lemmatize(token, tag_map[tag[0]]))

    return ' '.join(str(e) for e in filtered_sentence)


def cleaning(data):
    tag_map = nltk.defaultdict(lambda: wordnet.NOUN)
    tag_map['J'] = wordnet.ADJ
    tag_map['V'] = wordnet.VERB
    tag_map['R'] = wordnet.ADV

    remove = string.punctuation
    punctuation_re = re.compile('[' + remove + ']')
    # print(data)
    # Handle dates
    data = date.extract_dates(data)
    # remove useless characters
    tokens1 = punctuation_re.sub(' ', data)

    # split to tokens
    tokens = word_tokenize(tokens1.lower())

    # handle dates (convert to general format)
    # tokens = handle_dates(tokens)

    stop_words = set(stopwords.words('english'))

    filtered_sentence = []

    for token, tag in nltk.pos_tag(tokens):
        if token not in stop_words:
            # short_cut = shortcuts.get(token)
            # if short_cut:
            #     token = short_cut

            if (token != '.') & (token != '-') & (token != '/'):
                filtered_sentence.append(wordnet_lemmatizer.lemmatize(token, tag_map[tag[0]]))
    filtered_sentence = ' '.join(str(e) for e in filtered_sentence)
    return filtered_sentence


def is_ascii(s):
    return all(ord(c) < 128 for c in s)


def convert_numbers(data):
    tokens = word_tokenize(str(data))
    new_text = ""
    for w in tokens:
        try:
            w = num2words(int(w))
        except:
            a = 0
        new_text = new_text + " " + w
    new_text = np.char.replace(new_text, "-", " ")
    return new_text


def handle_special_cases(data):
    new_text = ""
    tokens = word_tokenize(str(data))
    # tokens = date.handle_dates(tokens)
    for token in tokens:
        short_cut = shortcuts.get(token)
        if short_cut:
            token = short_cut
        else:
            for fmt in date.fmts:
                try:
                    t = date.dt.datetime.strptime(token, fmt)
                    token = str(t).split(' ')[0]
                    break
                except ValueError as err:
                    pass
        new_text = new_text + " " + token

    new_text = np.char.replace(new_text, "-", " ")
    return new_text


def get_sound(query):
    s = soundex.getInstance()
    sou = str(query)
    sou = sou.split(" ")
    for t in sou:
        if t != '':
            query = query.replace(str(t), str(s.soundex(t)))
    return query.lower()


def nGramsHandler(terms, min_occurance=3):

    gram2 = list(nltk.ngrams(terms, 2))
    gram2 = [g[0]+" "+g[1] for g in gram2 if not g[0]
             in stop_words and not g[1] in stop_words]

    gram3 = list(nltk.ngrams(terms, 3))
    gram3 = [g[0]+" "+g[1]+" "+g[2] for g in gram3 if not g[0]
             in stop_words and not g[2] in stop_words]

    gram4 = list(nltk.ngrams(terms, 4))
    gram4 = [g[0]+" "+g[1]+" "+g[2]+" "+g[3]
             for g in gram4 if not g[0] in stop_words and not g[3] in stop_words]

    grams = gram2 + gram3 + gram4
    grams = [g for g in grams if grams.count(g) >= min_occurance]

    return grams


def preprocess_modified(txt):

    txt = txt.lower()
    txt = txt.strip('.')
    word = ""
    final_tokens = []
    tokens = word_tokenize(txt)

    quantity_pattern = "((\d{1,3},\d{3})|(\d{1,3}))(-\w*)*"

    filtered_tokens = [w.strip(string.punctuation)
                       for w in tokens if not re.search(quantity_pattern, w)]
    filtered_tokens = [w for w in filtered_tokens if not w in list(
        string.punctuation) + stop_words and is_ascii(w) and len(w) > 1]

    multi_terms = nGramsHandler(filtered_tokens)

    tokens_with_pos = [
        w for w in nltk.pos_tag(filtered_tokens) if w[1][0] in ["V", "N", "J", "R"]
    ] + [
        (w, "Noun") for w in multi_terms
    ]

    for idx, (token, pos) in enumerate(tokens_with_pos):
        if (pos[0] == "V"):
            word = (wordnet_lemmatizer.lemmatize(token, "v"), "verb")
        elif (pos[0] in ["R", "J"]):
            word = (wordnet_lemmatizer.lemmatize(token, "a"), "noun")
        else:
            word = (stemmer.stem(token), "noun")

        final_tokens.append(word)

    tokens = [token[0] for token in final_tokens]

    return tokens

