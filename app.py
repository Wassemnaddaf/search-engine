import flask
from flask import Flask, request, render_template, send_from_directory, flash, redirect, url_for
import json, os
from werkzeug.utils import secure_filename
from flask_cors import CORS
from evaluation import test,evaluate_query
import index2
# from test2 import auto_complete
# from test2 import cosine_similarity

app = Flask(__name__)
CORS(app)

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'js', 'css'])

app.secret_key = 'SearchEngine'
app.config['SESSION_TYPE'] = 'filesystem'


@app.route('/assets/<path:path>')
def sendAsset(path):
    return send_from_directory('static', path)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/auto_complete/<word>')
def auto_complete(word):
    print(word)
    response = flask.jsonify(index2.auto_complete(word))
    # response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/search')
def search():
    query = request.args.get('query')
    print(query)
    data = index2.cosine_similarityy(10, query)
    corrections = index2.get_query_correction(query)
    evaluate, avg_pre = evaluate_query(query, similarity=data)
    response = {'data': data, 'correct': corrections, 'evaluate': evaluate, 'average': avg_pre}
    response = {'data': data, 'correct': corrections}
    print(response)
    # response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/test')
def test_all():
    docs = request.args.get('k')
    data_vsm, data_bm25, avg_vsm, avg_bm25, avg_pre_vsm, avg_pre_bm25 = test(docs)
    response = {'vsm': data_vsm, 'bm25': data_bm25, "avg_vsm": avg_vsm, "avg_bm25": avg_bm25, "avg_pre_vsm": avg_pre_vsm, "avg_pre_bm25": avg_pre_bm25 }
    # response.headers.add('Access-Control-Allow-Origin', '*')
    return response



@app.route('/corpus/<path:path>')
def sendDoc(path):
    return send_from_directory('corpus', path)


if __name__ == '__main__':
    app.run()
