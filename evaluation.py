from index2 import cosine_similarityy
from BM25 import get_similarity


def loadFile(path):
    try:
        file = open(path, "r")
        data = file.read()
        file.close()
        return data
    except:
        print("EXCEPTION: while load file: " + path)
        return False


def load_queries():
    queries = loadFile('files/queries.txt')
    return [q for q in queries.split('\n') if q]


def load_relevance():
    text = loadFile('./files/relevance.txt')
    relevances = [r for r in text.split('\n') if r]
    final_relevance = [relevance.split() for relevance in relevances]
    return final_relevance


def evaluate_query(query, similarity):
    relevances = load_relevance()
    queries = load_queries()
    evaluates = []
    relevance_cnt = 0
    try:
        query_idx = queries.index(query)
    except:
        return False
    avg_pre = 0
    if len(similarity) > 0 and (not isinstance(similarity[0], int) and not isinstance(similarity[0], str)):
        results = [str(r.get('file')) for r in similarity]
    else:
        results = similarity
    for i in range(len(results)):
        if str(results[i]) in relevances[query_idx]:
            relevance_cnt = relevance_cnt + 1
        precision, recall, fscore = calc_fscore(relevances[query_idx], relevance_cnt, i + 1)
        evaluates.append({"precision": precision, "recall": recall, "fscore": fscore})
    for sc in evaluates:
        avg_pre += float(sc.get("precision"))
    if relevance_cnt != 0:
        avg_pre = float(avg_pre) / float(relevance_cnt)
    return evaluates, avg_pre


def calc_fscore(relevance, relevance_cnt, k):
    precision = float(relevance_cnt) / float(k)
    recall = float(relevance_cnt) / float(len(relevance))
    fscore = 0
    try:
        fscore = 2 * precision * recall / precision * recall
    except:
        pass
    return precision, recall, fscore


def test(k):
    queries = load_queries()
    relevances = load_relevance()
    avg_pre_vsm = 0
    avg_pre_bm25 = 0
    scores_vsm = []
    scores_bm25 = []
    for query, relevance in zip(queries, relevances):
        results_vsm = cosine_similarityy(int(k), query.lower())
        results_bm25 = get_similarity(int(k), query.lower())
        results_vsm = [str(r.get('file')) for r in results_vsm]
        relevance_cnt_vsm = 0
        relevance_cnt_bm25 = 0
        for r in results_vsm:
            if r in relevance:
                relevance_cnt_vsm = relevance_cnt_vsm + 1
        for r in results_bm25:
            if str(r) in relevance:
                relevance_cnt_bm25 = relevance_cnt_bm25 + 1
        precision, recall, fscore = calc_fscore(relevance, relevance_cnt_vsm, k)
        scores_vsm.append({"precision": precision, "recall": recall, "fscore": fscore})

        precision2, recall2, fscore2 = calc_fscore(relevance, relevance_cnt_bm25, k)
        scores_bm25.append({"precision": precision2, "recall": recall2, "fscore": fscore2})
        x, y = evaluate_query(query, results_vsm)
        avg_pre_vsm += float(y)
        x, y = evaluate_query(query, results_bm25)
        avg_pre_bm25 += float(y)

    avg_vsm = 0
    avg_bm25 = 0
    for sc in scores_vsm:
        avg_vsm += float(sc.get("fscore"))
    for sc in scores_bm25:
        avg_bm25 += float(sc.get("fscore"))
    avg_bm25 = float(avg_bm25) / float(len(queries))
    avg_vsm = float(avg_vsm) / float(len(queries))
    avg_pre_bm25 = float(avg_pre_bm25) / float(len(queries))
    avg_pre_vsm = float(avg_pre_vsm) / float(len(queries))
    return scores_vsm, scores_bm25, avg_vsm, avg_bm25, avg_pre_vsm, avg_pre_bm25
