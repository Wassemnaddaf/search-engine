import nltk
# from spellchecker import SpellChecker
from nltk.tokenize import word_tokenize


def get_count(token):
    return token.get('count')


def spelling_check(tokens, word):
    if word not in tokens:
        return get_corrections(tokens, word)
    else:
        return []


def get_corrections(tokens, word):
    corrections = []
    for token in tokens:
        if nltk.edit_distance(token, word) <= 2:
            corrections.append({"token": token, "count": tokens[token]})
    corrections.sort(key=get_count, reverse=True)
    return corrections


# spell = SpellChecker()
#
#
# def get_suggested_query(query):
#     query_tokens = word_tokenize(query)
#     misspelled = spell.unknown(query_tokens)
#     for word in misspelled:
#         query = query.replace(word, spell.correction(word))
#     return query
#
