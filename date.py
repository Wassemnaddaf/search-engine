import datefinder
import re
import datetime as dt

fmts = ('%Y', '%b %d, %Y', '%b %d, %Y', '%B %d, %Y', '%B %d %Y', '%d/%m/%Y', '%m/%d/%Y', '%m/%d/%y', '%b %Y', '%B%Y',
        '%b %d,%Y')


def handle_dates(tokens):

    for i in range(len(tokens)):
        for fmt in fmts:
            try:
                t = dt.datetime.strptime(tokens[i], fmt)
                tokens[i] = str(t).split(' ')[0]
                break
            except ValueError as err:
                pass

    return tokens


def extract_dates(txt):
    pattern = "((\d\$)|(\d+-\w*)|(\$\d+))|(\.)"
    txt = re.sub(pattern, '', txt)
    matched_dates = datefinder.find_dates(
        txt, False, False, False, dt.datetime(5555, 12, 1))
    formatted_dates = []
    if matched_dates:
        for date in matched_dates:
            try:
              formatted_dates.append(date.strftime("%d-%m-%Y"))
            except:
                pass
    return formatted_dates
