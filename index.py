# import these modules

from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from nltk.corpus import wordnet
import math

print(wordnet.synsets('better'))
ps = PorterStemmer()

lemmatizer = WordNetLemmatizer()



def get_vocabulary(documents):
    vocabulary = set([word for document in documents for word in document])
    return vocabulary


def words_count_in_docs(documents, word):
    counter = 0
    for document in documents:
        if word in document:
            counter += 1

    return counter


# compute TF
def term_frequency(wordDict, bagOfWords):
    tfDict = {}
    bagOfWordsCount = len(bagOfWords)
    for word, count in wordDict.items():
        tfDict[word] = count / float(bagOfWordsCount)
    return tfDict


# Compute IDF
def inverse_document_frequency(documents):

    document_cnt = len(documents)
    idfDict = dict.fromkeys(documents[0].keys(), 0)
    print(idfDict)
    for document in documents:
        for word, val in document.items():
            if val > 0:
                idfDict[word] += 1

    for word, val in idfDict.items():
        idfDict[word] = math.log(document_cnt / float(val))
    return idfDict


def compute_tf_idf(tfBagOfWords, idfs):
    tfidf = {}
    tfidf_vector = []
    for word, val in tfBagOfWords.items():
        tfidf[word] = val * idfs[word]
        tfidf_vector.append(val * idfs[word])
    return tfidf, tfidf_vector


def build_index(corpus):
    num_of_words_all = []
    tf_all = []
    unique_words = set()

    for document in corpus:
        unique_words = unique_words.union(set(document))

    for document in corpus:
        num_of_words = dict.fromkeys(unique_words, 0)

        for word in document:
            num_of_words[word] += 1
        tf = term_frequency(num_of_words, document)
        tf_all.append(tf)
        num_of_words_all.append(num_of_words)
    idf = inverse_document_frequency(num_of_words_all)
    vectors = []
    result = []
    for document in range(len(corpus)):
        tf_idf, vector = compute_tf_idf(tf_all[document], idf)
        vectors.append(vector)
        result.append(tf_idf)
    return result, vectors
