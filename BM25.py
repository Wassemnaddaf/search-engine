import glob
from utils import *
from nltk import word_tokenize
from rank_bm25 import BM25Okapi
from index2 import *
N = 423


# extract_data()
bm25 = BM25Okapi(tokenized_corpus_modified)


def get_similarity(k, query):
    # tokenized_query = word_tokenize(preprocess(query))
    tokenized_query = preprocess_modified(query)
    # doc_scores = bm25.get_scores(tokenized_query)
    results = bm25.get_top_n(tokenized_query, corpus, n=k)
    files = []
    for result in results:
        files.append(corpus.index(result) + 1)

    return files





